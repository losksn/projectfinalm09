/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectom9.criptografia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author KatherinDenis
 */
public class Simetrico {

    private FileOutputStream fos = null;
    private FileInputStream fis = null;
    private final String algoritmo = "AES/ECB/PKCS5Padding";
    private String blue = "\u001B[34m", reset = "\u001B[0m";

    /**
     *
     */
    public Simetrico() {
    }

    /**
     * Muestra las opciones que se puedan realizar
     * @throws IOException
     * @throws FileNotFoundException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     */
    public void Opciones() throws IOException, FileNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        //Para que no nos aparezca la excepcion de limited police
        //Se tienen que descargar los jce.polize-8.zip
        //Estos 2 archivos se remplazaran en las carpetas jdk/jre/lib/security
        System.out.println(blue + "-----------Criptografia Simétrica-----------" + reset);
        boolean on = true;
        while (on) {
            System.out.println("\nIntroduce una opción:");
            System.out.println("-----------------------");
            System.out.println("1)Cifra un fichero");
            System.out.println("2)Descifra un fichero");
            System.out.println("3)Salida");
            switch ((ingresoOpcionesMenu())) {
                case 1:
                    cifraDocumentoAES();
                    break;
                case 2:
                    descifrarDocumentoAES();
                    break;
                case 3:
                    System.out.println(blue + "Cerrando criptografia..." + reset);
                    on = false;
                    break;
                default:
                    System.out.println(blue + "Está  opción no es válida" + reset);
            }
        }
    }

    /**
     *Genera la Llave AES
     * @param password
     * @param keySize
     * @return
     */
    public SecretKey generadorLLaveAES(String password, int keySize) {
        SecretKey sKey = null;
        if ((keySize == 128) || (keySize == 192) || (keySize == 256)) {
            try {
                byte[] data = password.getBytes("UTF-8");
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                byte[] hash = md.digest(data);
                byte[] key = Arrays.copyOf(hash, keySize / 8);
                sKey = new SecretKeySpec(key, "AES");
            } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
                System.err.println(blue + "Error generando la llave:" + reset + ex);
            }
        }
        return sKey;
    }

    /**
     *Cifra el documento elegido con la llave aes
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void cifraDocumentoAES() throws NoSuchAlgorithmException, NoSuchPaddingException, FileNotFoundException, IOException {
        SecretKey claveSecreta = null;
        File f = new File("crypt/" + ingresaNomFichero());
        if (f.exists()) {
            try {
                System.out.println(blue + "Introduce un password que desea utilizar para cifrar:" + reset);
                String password = ingresoPassword();
                //Crea la llave desde el password proporcionado
                claveSecreta = generadorLLaveAES(password, 256);

                //Encriptador
                Cipher cipher = Cipher.getInstance(algoritmo);
                cipher.init(Cipher.ENCRYPT_MODE, claveSecreta);

                //Archivo a encriptar
                fis = new FileInputStream(f);
                //El archivo encriptado se guardará  con la extension .aes
                fos = new FileOutputStream(f + ".aes");

                CipherOutputStream cos = new CipherOutputStream(fos, cipher);
                int medida = cipher.getBlockSize();
                byte buf[] = new byte[medida];
                int i = fis.read(buf);
                while (i != -1) {
                    cos.write(buf, 0, i);
                    i = fis.read(buf);
                }
                cos.flush();
                cos.close();
                fis.close();
                System.out.println(blue + "Documento encriptado con el password." + reset);
            } catch (InvalidKeyException ex) {
                System.out.println(blue + "Invalid Key Exception:" + reset + ex);
            }

        } else {
            System.out.println(blue + "El archivo no se encunetra en la carpeta." + reset);
        }
    }

    /**
     *Descifra el documento que hacido cifrado 
     * debe tener una terminación .aes
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void descifrarDocumentoAES() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, FileNotFoundException, IOException {
        //Documento a descifrar
        File f = new File("crypt/" + ingresaNomFichero());
        if (f.exists()) {
            System.out.println(blue + "Introduce el password para descifrar:" + reset);
            //si el password no coincide creará el archivo pero sin contenido 
            //el password tiene que ser el que se uso en Cifrardocumento.
            String password = ingresoPassword();
            Key claveSecreta = generadorLLaveAES(password, 256);
            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.DECRYPT_MODE, claveSecreta);

            fis = new FileInputStream(f);
            //si se descomenta se podra ver la imagen descifrada
            //fos = new FileOutputStream("descifrado.jpg");
            fos = new FileOutputStream(f + ".clar");
            CipherOutputStream cos = new CipherOutputStream(fos, cipher);
            int medida = cipher.getBlockSize();
            byte buf[] = new byte[medida];
            int i = fis.read(buf);
            while (i != -1) {
                cos.write(buf, 0, i);
                i = fis.read(buf);
            }
            cos.flush();
            cos.close();
            fis.close();
            System.out.println(blue + "Documento descifrado....." + reset);

        } else {
            System.out.println(blue + "El archivo no se encuentra en el fichero." + reset);
        }
    }

    private String ingresoPassword() {
        Scanner teclado = new Scanner(System.in);
        String pass = teclado.nextLine();
        return pass;
    }

    /**
     *El fichero puede ser normal, osea cualquier terminación
     * o si es para decifrar tendra un terminación .aes
     * @return String nomfitxer 
     */
    public String ingresaNomFichero() {
        Scanner teclado = new Scanner(System.in);
        System.out.println(blue + "Ingrese el nombre del fichero: (Esté se buscara en el fichero crypt/)ejemplo 'exe.jpg')" + reset);
        String nomfitxer = teclado.next();
        return nomfitxer;
    }

    private int ingresoOpcionesMenu() {
        Scanner teclado = new Scanner(System.in);
        int opcion = 0;
        if (teclado.hasNextInt()) {
            opcion = teclado.nextInt();
        } else {
            System.out.println(blue + "No es un valor válido" + reset);
        }
        return opcion;
    }

    /**
     *Genera un par de LLaves RSA 
     * @return KeyPair
     * @throws NoSuchAlgorithmException
     */
    public KeyPair generadorLlaveAsimetrica() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(1024);
        KeyPair keypar = keyPairGenerator.generateKeyPair();
        return keypar;
    }
}
