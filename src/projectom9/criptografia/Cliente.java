package projectom9.criptografia;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 *
 * @author KatherinDenis
 */
public class Cliente {

    private final String algoritmo = "RSA/ECB/PKCS1Padding";
    /**
     */
    public static Simetrico simetrico = new Simetrico();

    /**
     *
     * @param args
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InterruptedException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     */
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InterruptedException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException {
        Scanner teclado = new Scanner(System.in);
        String host = "localhost";
        int port = 10000;
        SecretKey secretKey;

        Cliente c = new Cliente();

        try {
            Socket s = new Socket(host, port);
            DataOutputStream dos = new DataOutputStream(s.getOutputStream());
            ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream ois = new ObjectInputStream(s.getInputStream());

            //Al iniciar el Cliente enviara el nombre del cliente, asi como la KeySecret-Wrap 
            System.out.println("------ Bienvenido------");
            System.out.println("Dinos tu nombre:");
            String nom = teclado.nextLine();
            //envia nombre al servidor
            dos.writeUTF(nom);

            //Recibe la llavePublica del Server
            PublicKey publicKey = (PublicKey) ois.readObject();
            System.out.println("0---------Recibido LlavePublica de SERVER------");
            boolean off = true;
            while (off) {
                c.opcionesCliente();
                int op = teclado.nextInt();
                //Envia opcion
                dos.writeInt(op);
                switch (op) {
                    case 1:
                        //Genera la Llave AES el nombre del cliente sera su 'password', 
                        //se podria dar la opcion de pedir un password o que nos genere uno automático
                        secretKey = simetrico.generadorLLaveAES("llave para del cliente" + nom, 256);
                        System.out.println("1--------Genera Llave AES del CLiente-------");
                        byte[] llaveAESRSA = c.wrapSecretaiPublica(publicKey, secretKey);
                        //Se envia AES_RSA-Wrap al Server
                        oos.writeObject(llaveAESRSA);
                        System.out.println("2---------Enviando LLAVE AES-RSA al SERVER------");

                        //Crea el fichero para la AES_RSA_Wrap, estara en /crypt
                        c.creacionFichero(nom, llaveAESRSA);

                        //Encripta el fichero, pide el nombre del fichero a encriptar
                        String FileAES = c.encriptacionAES(secretKey, nom);
                        //Enviar ficheroAES
                        dos.writeUTF(FileAES);
                        System.out.println("\t3---------Enviando FicheroAES al SERVER-------");

                        System.out.println("\nOperaciones Realiadas.");
                        break;
                    case 2:
                        dos.writeInt(op);
                        System.out.println("Sesion finalizada con el Servidor");
                        System.out.println("Adios!");
                        off = false;

                        break;
                    default:
                        System.out.println("Esta opción no es válida");
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     *Crea el fichero donde se guardará la llave envuelta con rsa
     * @param nom
     * @param filebytes
     * @throws IOException
     */
    public void creacionFichero(String nom, byte[] filebytes) throws IOException {
        FileOutputStream fos = new FileOutputStream("crypt/" + nom + ".AesWrapRsa.pswd");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.write(filebytes, 0, filebytes.length);
        oos.close();
    }

    /**
     * Menu de opciones que tendra el cliente
     */
    public void opcionesCliente() {
        System.out.println("------Servicio Asimétrico------");
        System.out.println("1)Selecciona el archivo");
        System.out.println("2)Cerrar Conexión");
    }

    /**
     * Envuelve la llave Aes con la llave pública rsa
     *
     * @param publicKey
     * @param sk
     * @return
     * @throws InvalidKeyException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws IllegalBlockSizeException
     */
    public byte[] wrapSecretaiPublica(PublicKey publicKey, SecretKey sk) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(algoritmo);
        cipher.init(Cipher.WRAP_MODE, publicKey);
        byte clauEmbolcallada[] = cipher.wrap(sk);
        return clauEmbolcallada;
    }

    /**
     * revisará que el archivo escogido exista, lo encriptará con su llave AES 
     * y lo guarda con la extendion (nameFile+ nom+.aes)
     * @return fname: nombre del archivo 
     * @param secretKey
     * @param nom
     * @return String fname ruta del archivo
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public String encriptacionAES(SecretKey secretKey, String nom) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, FileNotFoundException, IOException {
        byte[] bytes = null;
        String fname = null;
        //Encriptar documento con AES
        //busca el fichero a encriptar
        File f = new File("crypt/" + simetrico.ingresaNomFichero());
        if (f.exists()) {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            //Archivo escogido para encriptar f
            FileInputStream fis = new FileInputStream(f);
            //El archivo encriptado se guardará  con la extension .aes
            FileOutputStream fos = new FileOutputStream(f + nom + ".aes");

            try (
                    CipherOutputStream cos = new CipherOutputStream(fos, cipher)) {
                int medida = cipher.getBlockSize();
                bytes = new byte[medida];
                int i = fis.read(bytes);
                while (i != -1) {
                    cos.write(bytes, 0, i);
                    i = fis.read(bytes);
                }

                cos.flush();
                cos.close();
                fis.close();
            }
            //retorna la posicion del documento
            fname = f + nom + ".aes";
            System.out.println("-----Fichero encripta con la Llave AES del Cliente-----");

        } else {
            System.out.println("El archivo no se encuentra en la carpeta crypt.");
        }
        return fname;
    }

}
