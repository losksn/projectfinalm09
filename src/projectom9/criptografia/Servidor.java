package projectom9.criptografia;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author KatherinDenis
 */
public class Servidor extends Thread {

    private final String algoritmo = "RSA/ECB/PKCS1Padding";

    /**
     *
     */
    public static int cont = 0;

    /**
     *
     */
    public static Socket s;
    static Simetrico simetrico = new Simetrico();
    static KeyPair kp;

    /**
     *
     * @param args
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InterruptedException {
        int puerto = 10000;
        ServerSocket ss = new ServerSocket(puerto);
        kp = simetrico.generadorLlaveAsimetrica();
        //Genera las llaves del Servidor
        boolean off = false;
        System.out.println("------Servidor Asimétrico ------\nesperando conexión....");
        while (!off) {
            new Servidor((Socket) ss.accept()).start();
            cont++;
        }

    }
    private ObjectInputStream ois = null;
    private ObjectOutputStream oos = null;

    /**
     *
     * @param s
     */
    public Servidor(Socket s) {
        this.s = s;
    }

    @Override
    public void run() {
        try {
            DataInputStream dis = new DataInputStream(s.getInputStream());
            oos = new ObjectOutputStream(s.getOutputStream());
            ois = new ObjectInputStream(s.getInputStream());
            //Recibe el nombre del cliente
            String nom = dis.readUTF();
            boolean opciones = true;
            System.out.println(" Conexión #:" + cont + " Cliente:" + nom + "\n" + s.getInetAddress());
            //Envia la LlavePublica Server al CLiente
            oos.writeObject(kp.getPublic());
            System.out.print("\t--------Enviando ClavePublica------->" + nom + "\n");

            //recibe la opcion desde el Cliente
            while (opciones) {

                int op = dis.readInt();
                System.out.println("-----------------------------------------");
                switch (op) {
                    case 1:
                        // Recibe AES_RSA,la guarda, esta con el nombre de (nom+AesWrapRsa.pswd)
                        byte[] SecretWrapRsa = (byte[]) ois.readObject();
                        System.out.print("1--------------AES_RSA_WRAP Recibido del Cliente-------" + nom + "\n");

                        //Recibe la posicion del archivo Encriptado del Cliente, el cliente tiene que escoger su archivo
                        String FileEncriptado = dis.readUTF();
                        System.out.println("2------------ArchivoAES del Cliente Recibido!--------" + nom);

                        //Desencripta Llave AES_RSA
                        Key descifradoAES = descencriptarAES_RSA((byte[]) SecretWrapRsa);
                        System.out.println("3------------Llave AES del cliente " + nom + " descencriptada.---------" + descifradoAES.getAlgorithm());
                        //Descifrar fichero, asi como creara su archivo con el patron de 'descifrado_nomCLiente.jpg'
                        //jpg porque he hecho las pruebas con estás.
                        descifrarFicheroAES(descifradoAES, FileEncriptado, nom);///
                        System.out.println("4---------Descifrado Fichero AES " + nom + "-----------");
                        System.out.println("Operaciones realizadas........");
                        break;
                    case 2:
                        //Cierra la conexion del socket
                        opciones = false;
                        dis.close();
                        s.close();
                        ois.close();
                        oos.close();
                        System.out.println("\t Conexión cerrada con ( " + nom + s.getInetAddress() + ':' + s.getPort() + ')');
                        System.out.println("");
                        break;
                    default:
                        System.out.println("La opción no es válida");
                }
            }
        } catch (IOException ex) {
            System.out.println("IOException-->" + ex);
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("NoSuchAlgorithmException-->" + ex);
        } catch (NoSuchPaddingException ex) {
            System.out.println("NoSuchPaddingException-->" + ex);
        } catch (InvalidKeyException ex) {
            System.out.println("InvalidKeyException-->" + ex);
        } catch (IllegalBlockSizeException ex) {
            System.out.println("IllegalBlockSizeException-->" + ex);
        } catch (BadPaddingException ex) {
            System.out.println("BadPaddingException-->" + ex);
        } catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException-->" + ex);
        }

    }

    /**
     *
     * @param LlaveAESRSA
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     */
    public Key descencriptarAES_RSA(byte[] LlaveAESRSA) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance(algoritmo);
        cipher.init(Cipher.UNWRAP_MODE, kp.getPrivate());
        Key AesUnwrap = cipher.unwrap(LlaveAESRSA, "AES", Cipher.SECRET_KEY);
        return AesUnwrap;
    }

    /**
     *
     * @param aesUwrap
     * @param fileAes
     * @param nom
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void descifrarFicheroAES(Key aesUwrap, String fileAes, String nom) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, FileNotFoundException, IOException {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, aesUwrap);
        try (FileInputStream fis = new FileInputStream(fileAes)) {
            FileOutputStream fos = new FileOutputStream("descifrado_" + nom + ".jpg");
            CipherOutputStream cos = new CipherOutputStream(fos, cipher);
            int medida = cipher.getBlockSize();
            byte[] bytes = new byte[medida];
            int i = fis.read(bytes);
            while (i != -1) {
                cos.write(bytes, 0, i);
                i = fis.read(bytes);
            }
            cos.flush();
            cos.close();
        }

    }

}
