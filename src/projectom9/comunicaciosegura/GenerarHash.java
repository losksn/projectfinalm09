/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectom9.comunicaciosegura;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import static projectom9.comunicaciosegura.Client.p;
import static projectom9.comunicaciosegura.Client.teclat;

/**
 *
 * @author Salva
 *
 */
public class GenerarHash {

    private static String m = "";

    /**
     * Metode per crear el hash i que ens retorni el resum .
     *
     * @param hash En quina codificació és fa.
     * @param arxiu L'arxiu del qual s'ha de fer el resum
     * @param ruta Parametre el qual ens dona part d'informacio d'on es guardara
     * l'arxiu nou generat.
     * @return Retorna la funció hash generada.
     */
    public String crearhash(String hash, String arxiu, String ruta) {

        try {

            MessageDigest messageDigest2 = MessageDigest.getInstance(hash); // Inicializa SHA-1

            //leer fichero byte a byte
            try {

                InputStream archivo = new FileInputStream("hash/" + ruta + arxiu);

                byte[] buffer = new byte[1];
                int fin_archivo = -1;
                int caracter;

                caracter = archivo.read(buffer);

                while (caracter != fin_archivo) {
                    messageDigest2.update(buffer);
                    caracter = archivo.read(buffer);
                }
                archivo.close();
                byte[] resumen2 = messageDigest2.digest(); // Genera el resumen SHA-1

                for (int i = 0; i < resumen2.length; i++) {
                    m += Integer.toHexString((resumen2[i] >> 4) & 0xf);
                    m += Integer.toHexString(resumen2[i] & 0xf);
                }
                System.out.println("Resumen SHA-1: " + m);

            } //lectura de los datos del fichero
            catch (java.io.FileNotFoundException fnfe) {
            } catch (java.io.IOException ioe) {
            }

        } //declarar funciones resumen
        catch (java.security.NoSuchAlgorithmException nsae) {
        }

        return m;
    }

    /**
     * Metode que genera l'arxiu amb el resum hash.
     * @param hash En quina codificació és fa.
     * @param arxiu L'arxiu del qual s'ha de fer el resum
     * @param ruta Parametre el qual ens dona part d'informacio d'on es guardara
     * l'arxiu nou generat.
     * @return Ens retorna el resum del hash.
     * @throws NoSuchAlgorithmException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public String generacionhash(String hash, String arxiu, String ruta) throws NoSuchAlgorithmException, FileNotFoundException, IOException {
        Random ran = new Random();

        String nomHash = p.generateRandomWord(ran.nextInt(15) + 3) + ".dat";

        //CREA UN NOU FITXER AMB EL HASH
        File fH = new File("hash/" + ruta + nomHash);
        RandomAccessFile arxiuH = new RandomAccessFile(fH, "rw");
        System.out.println(arxiu + hash);
        String m = crearhash(hash, arxiu, ruta);
        arxiuH.writeUTF(m);
        return m;

    }
    /**
     * Metode per  saber quin hash utilizar.
     * @return Retorna un string amb quin sera el hash que es té que utilitzar concretament.
     */

    public String codeHash() {
        String s[] = {"SHA-128", "SHA-256", "SHA-512"};
        Random rand = new Random();
        int i = rand.nextInt(3);
        return s[i];
    }
/**
 * Metode per crear paraules aleatories.
 * @param wordLength Parametre en el qual es passa un numero per crear 
 * la longitud de la paraula.
 * @return Retorna una paraula inventada aleatoriament.
 */
    String generateRandomWord(int wordLength) {
        Random r = new Random(); // Intialize a Random Number Generator with SysTime as the seed
        StringBuilder sb = new StringBuilder(wordLength);
        for (int i = 0; i < wordLength; i++) { // For each letter in the word
            char tmp = (char) ('a' + r.nextInt('z' - 'a'));
// Generate a letter between a and z
            sb.append(tmp); // Add it to the String
        }
        return sb.toString();
    }
    /**
     * 
     * @param hash1 Parametre que té el resum del hash.
     * @param hash2 Parametre que té el resum del hash.
     * @return  Retorna un boolean true o exit depenen si la condició la ha fet.
     * 
     */

    public boolean comprovaciohash(String hash1, String hash2) {
        boolean exit = false;
        if (hash1.equals(hash2)) {

            exit = true;
        }

        return exit;
    }

 

}
