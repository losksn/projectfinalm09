/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectom9.comunicaciosegura;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

/**
 *
 * @author Salva
 * 
 */
public class Servidor extends Thread {

    public static int con = 0;
    public static String codificacio;
    public static String fitxer;
    public static String ruta="servidor/";
    public static GenerarHash hash = new GenerarHash();

    public static void main(String[] args) throws IOException {
        System.setProperty("javax.net.ssl.keyStore", "certificate/ServerSSLKeyStore");
        System.setProperty("javax.net.ssl.keyStorePassword", "123456");

        ServerSocketFactory ssf = SSLServerSocketFactory.getDefault();
        ServerSocket ss = ssf.createServerSocket(15000);
        System.out.println("Servidor esperant connexió");
        while (true) {
            
            new Servidor((SSLSocket) ss.accept()).start();
           
            con++;
            
        }
       
    }
    private SSLSocket sock;

    public Servidor(SSLSocket s) {
        sock = s;

    }

    @Override
    public void run() {
        try {
            DataInputStream in = new DataInputStream(sock.getInputStream());
            DataOutputStream out = new DataOutputStream(sock.getOutputStream());
            boolean exit = true;
            String nom = in.readUTF();
            System.out.println("Client connectat al servidor.");
            System.out.println("El client :" + nom + ".\t Connexió:" + con);
            while (exit) {

                int op = in.readInt();
                switch (op) {
                    case 1:
                        //COMENÇA DIALOG AMB EL CLIENT.
                        codificacio=hash.codeHash();
                        out.writeUTF(codificacio);
                        fitxer=in.readUTF();
                        break;
                    case 2:

                        boolean ex=
                        hash.comprovaciohash(hash.generacionhash(codificacio, fitxer,ruta) ,in.readUTF());
                        out.writeBoolean(ex);
                        break;
                    case 3:
                        System.out.println("El client s'ha desconnectat");
                        in.close();
                        out.close();
                        sock.close();
                        exit = false;
                        break;
                    default:
                  
                }


            }

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
