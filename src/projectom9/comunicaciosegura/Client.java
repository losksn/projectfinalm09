/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectom9.comunicaciosegura;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/**
 *
 * @author Salva
 */
public class Client {

    public static Scanner teclat = new Scanner(System.in);
    public static MessageDigest md;
    //private static String nomFitxer;
    public static GenerarHash p = new GenerarHash();
    public static Client c = new Client();
    private static String fitxer, hash, hash1;
    private static String ruta = "client/";
    final static File carpeta = new File("hash/");

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InterruptedException {
        String host = "localhost";
        int port = 15000;
        boolean exit = false;
        System.setProperty("javax.net.ssl.trustStore", "certificate/UserSSLKeyStore");
        System.setProperty("javax.net.ssl.trustStorePassword", "654321");

        SSLSocketFactory ssf = (SSLSocketFactory) SSLSocketFactory.getDefault();
        SSLSocket clt = (SSLSocket) ssf.createSocket(host, port);
        DataInputStream in = new DataInputStream(clt.getInputStream());
        DataOutputStream out = new DataOutputStream(clt.getOutputStream());
        System.out.println("-----------");
        System.out.println("Identificat, Qui ets?");
        System.out.println("-----------");
        String str = teclat.nextLine();
        out.writeUTF(str);
        //COMENÇA DIALOG AMB EL SERVIDOR.
        while (!exit) {
            benvinguda();
            int opcio = teclat.nextInt();
            out.writeInt(opcio);
            switch (opcio) {
                case 1:

                    hash = in.readUTF();
                    fitxer = c.generaciofitxer();
                    out.writeUTF(fitxer);
                    hash1 = p.generacionhash(hash, fitxer, ruta);
                    break;

                case 2:
                    System.out.println("Comprovem si ha estat modificat.");
                    out.writeUTF(hash1);
                    if (in.readBoolean() == true) {
                        System.out.println("L'arxiu no ha estat modificat per ningú");
                    } else {
                        System.out.println("L'arxiu ha sigut alterat. Aneu amb compte.");
                    }
                    //comprobar
                    break;
                case 3:
                    System.out.print("Desconexió en 3.............");
                    Thread.sleep(1000);
                    System.out.print("...2.............");
                    Thread.sleep(1000);
                    System.out.print("...1.............");
                    Thread.sleep(1000);
                    in.close();
                    out.close();
                    clt.close();
                    exit = true;
                    break;
                default:
            }

        }

    }
/**
 * Metode per generar el fitxer.
 * @return
 * @throws FileNotFoundException
 * @throws IOException 
 */
    public String generaciofitxer() throws FileNotFoundException, IOException {
        System.out.println("----------------------");
        System.out.println("Crearem el fitxer!!");
        System.out.println("----------------------");
        teclat.nextLine();
        System.out.println("Escriu el missatge que es guardarà al fitxer.");
        String msg = teclat.nextLine();

        //Demana el nom del fitxer que guardarà el missatge
        System.out.println("Introdueix el nom del fitxer que emmagatzemarà el missatge.");
        String nomFitxer = teclat.next();

        //CREA UN NOU FITXER I EMMAGATZEMA EL MISSATGE
        File f = new File("hash/fitxers/" + nomFitxer+".txt");
        RandomAccessFile arxiu = new RandomAccessFile(f, "rw");
        arxiu.writeUTF(msg);

        return nomFitxer;

    }
    /**
     * 
     */

    public static void benvinguda() {
        System.out.println("--------------------------------------");
        System.out.println("-------BENVINGUT ESTIMAT CLIENT-------");
        System.out.println("--------------------------------------");
        System.out.println("\n\n\nEstem a la seva disposició. ");
        System.out.println("Que vol fer?");
        System.out.println("1.-Crear fixter i genera la funció hash.");
        System.out.println("2.-Comprobar l'arxiu encriptat.");
        System.out.println("3.-Sortir.");
        System.out.println("--------------------------------------");

    }

}
