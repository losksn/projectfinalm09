/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectom9;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import javax.crypto.NoSuchPaddingException;
import projectom9.criptografia.Simetrico;
import projectom9.hash.Hash;

/**
 *
 * @author Nerea
 */
public class ProjectoM9 {

    /**
     * @param args the command line arguments
     * @throws java.lang.ClassNotFoundException
     * 
     * Per poder iniciar el servidor de SSL cal que executem els arixus independents , ja que son mains diferents .
     * Per poder iniciar el servidor de Criptografia segura tenim que executar el seus arxius pertinens.
     */
    public static void main(String[] args) throws ClassNotFoundException, IOException, NoSuchAlgorithmException, InterruptedException, FileNotFoundException, NoSuchPaddingException, InvalidKeyException {
        // TODO code application logic here
        Scanner teclado = new Scanner(System.in);
        int op;
        //  System.out.println("Vamos Equipo.");
        Hash h = new Hash();
        Simetrico s = new Simetrico();
        boolean exit = false;

        while (!exit) {
            System.out.println("--------------------------------------------------");
            System.out.println("Tria l'opció desitjada.");
            System.out.println("1.-Comprovar les funcions hash.");
            System.out.println("2.-Comprovar les funcions Criptografia Simetrica.");
            System.out.println("3.-Tancar");
            System.out.println("--------------------------------------------------");
            op = teclado.nextInt();
            switch (op) {

                case 1:
                    h.inicializar();
                    break;
                case 2:
                    s.Opciones();
                    break;
                case 3:
                    exit = true;
                    break;
            };

        }
        System.out.print("Desconexió en 3.............");
        Thread.sleep(1000);
        System.out.print("...2.............");
        Thread.sleep(1000);
        System.out.print("...1.............");
        Thread.sleep(1000);
    }

}
