/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectom9.hash;

import java.io.File;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Nerea
 */
public class Hash {

    /**
     *
     */
    public static final String RED = "\u001B[31m";

    /**
     *
     */
    public static Scanner teclat = new Scanner(System.in);

    /**
     *
     */
    public static MessageDigest md;

    /**
     *Creación HASH
     */
    public Hash() {

    }

    /**
     * Inicia el menu
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws InterruptedException
     */
    public void inicializar() throws NoSuchAlgorithmException, IOException, InterruptedException {
        md = MessageDigest.getInstance("SHA-256");

        System.out.println(RED + "--- OPERACIONS AMB FUNCIÓ HASH ---");
        int op = 1;
        while (op != 4) {
            opcions();
            op = teclat.nextInt();
            switch (op) {

                case 1:
                    generacioHash();
                    break;
                case 2:
                    comprovacioHash();
                    break;
                case 3:
                    comprovacioHash2();
                    break;
                case 4:
                    System.out.println("Sortint...");
                    TimeUnit.SECONDS.sleep(5);

                    break;
                default:
                    System.out.println("Aquesta opció no és vàlida.");

            }
        }
    }

    /**
     *
     */
    public void opcions() {
        System.out.println("Introdueix l'opció:");
        System.out.println("----------------------------");
        System.out.println("1) Generació i emmagatzematge de Hash");
        System.out.println("2) Comprovació de Hash: Introducció per teclat");
        System.out.println("3) Comprovació de Hash II: Utilitzant un fitxer");
        System.out.println("4) Sortir");

    }

    /**
     * Una vez pedido los datos, creará un fichero donde se almacena el HASH
     * @throws FileNotFoundException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    public void generacioHash() throws FileNotFoundException, IOException, NoSuchAlgorithmException {

        //Demana el nom del fitxer que guardarà el missatge
        System.out.println("Introdueix el nom del fitxer que emmagatzemarà el missatge. Per exemple 'missatge.dat' :");
        String nomFitxer = teclat.next();
        teclat.nextLine();

        System.out.println("Introdueix el nom del fitxer que emmagatzemarà el Hash del missatge. Per exemple 'missatgeHash.dat' :");
        String nomHash = teclat.next();
        teclat.nextLine();

        System.out.println("Introdueix el missatge. Per exemple 'Hola' :");
        String msg = teclat.nextLine();

        //CREA UN NOU FITXER I EMMAGATZEMA EL MISSATGE
        File f = new File("hash/" + nomFitxer);
        RandomAccessFile arxiu = new RandomAccessFile(f, "rw");
        arxiu.writeUTF(msg);

        //CREA EL HASH
        byte[] encodedHash = md.digest(msg.getBytes(StandardCharsets.UTF_8));
        //System.out.println(md.toString());

        //CREA UN NOU FITXER AMB EL HASH
        File fH = new File("hash/" + nomHash);
        RandomAccessFile arxiuH = new RandomAccessFile(fH, "rw");
        arxiuH.writeUTF(bytesToHex(encodedHash));

        System.out.println("El teu missatge és: " + msg);
        System.out.println("El HASH (" + md.getAlgorithm() + ") :" + bytesToHex(encodedHash));
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    /**
     * Se ingresa un HASH
     * Se ingresa el fichero que almacena el mensaje
     * Comprueba que el hash introducido concuerde con lo introducido
     * @throws IOException
     */
    public void comprovacioHash() throws IOException {
        System.out.println("Introdueix el HASH (format Hexadecimal).\nPer exemple 'c6cdce45df8c3c85c112d7f2371abe83f2be97a3b20f10e04ef40a85cf42abc3'");
        //String str = "c6cdce45df8c3c85c112d7f2371abe83f2be97a3b20f10e04ef40a85cf42abc3";
        String str = teclat.next();
        teclat.nextLine();
        System.out.println("Introdueix el nom del fitxer que emmagatzema el missatge. Per exemple 'missatge.dat' :");
        String rutaM = "hash/" + teclat.next(); //ruta del fitxer hash
        teclat.nextLine();
        File f = new File(rutaM);
        if (f.exists()) {
            String s = null;
            RandomAccessFile arxiu = new RandomAccessFile(f, "r");
            while (arxiu.getFilePointer() < arxiu.length()) {
                s = arxiu.readUTF();
            }
            //System.out.println(s);
            byte[] msg = md.digest(s.getBytes(StandardCharsets.UTF_8));
            //System.out.println(bytesToHex(msg));
            if (str.equals(bytesToHex(msg))) {
                System.out.println("El missatge no ha estat alterat.");
                System.out.println("El contingut és: " + s);
            }
        } else {
            System.out.println(RED + "No existeix cap fitxer amb aquest nom.");
        }
    }

    /**
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void comprovacioHash2() throws FileNotFoundException, IOException {
        listarFicheros();
        System.out.println("Introdueix el nom del fitxer que emmagatzema el missatge. Per exemple 'missatge.dat' :");
        String rutaM = "hash/" + teclat.next();
        teclat.nextLine();
        File f = new File(rutaM);
        if (f.exists()) {
            System.out.println("Introdueix el nom del fitxer que emmagatzema el Hash del missatge. Per exemple 'missatgeHash.dat' :");
            String rutaHash = "hash/" + teclat.next(); //ruta del fitxer hash
            teclat.nextLine();
            File fH = new File(rutaHash);
            if (f.exists()) {
                String s = null;
                RandomAccessFile arxiu = new RandomAccessFile(f, "r");
                while (arxiu.getFilePointer() < arxiu.length()) {
                    s = arxiu.readUTF();
                }
                String sH = null;

                RandomAccessFile arxiuH = new RandomAccessFile(fH, "r");
                while (arxiuH.getFilePointer() < arxiuH.length()) {
                    sH = arxiuH.readUTF();
                }
                byte[] msg = md.digest(s.getBytes(StandardCharsets.UTF_8));
                // System.out.println(bytesToHex(msg));
                // System.out.println(sH);
                if (bytesToHex(msg).equals(sH)) {
                    System.out.println("El missatge no ha estat alterat.\nEl seu contingut es: " + s);
                }
            } else {
                System.out.println(RED + "No existeix cap fitxer amb aquest nom.");
            }
        } else {
            System.out.println(RED + "No existeix cap fitxer amb aquest nom.");

        }
    }

    /**
     *Lista los ficheros de la carpeta hash/
     */
    public void listarFicheros() {
        // Aquí la carpeta donde queremos buscar
        String path = "hash";

        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {

            if (listOfFiles[i].isFile()) {
                for (int j = 0; j < 3; j++) {
                    //files = listOfFiles[i].getName();
                    System.out.println(listOfFiles[i].getName());
                }

            }
        }

    }

}
